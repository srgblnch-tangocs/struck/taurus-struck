# !/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2021 CELLS / ALBA Synchrotron, Cerdanyola del Valles, Spain
#
# ##### END GPL LICENSE BLOCK #####


from taurus.qt.qtgui.base import TaurusBaseComponent
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.display import TaurusLabel
from taurus.external.qt.QtCore import QRect
from taurus.external.qt.QtCore import Qt as QtCore_Qt
from taurus.external.qt.QtWidgets import (QGridLayout, QLabel, QScrollArea,
                                          QSpacerItem, QSizePolicy, QWidget)


class DllrfTable(TaurusWidget):
    _outer_layout = None
    _inner_layout = None
    _scroll = None
    _widget = None
    _table = None
    _up_spacer = None
    _down_spacer = None
    _left_spacer = None
    _right_spacer = None
    _size_policy = None

    _model = None

    def __init__(self, *args, **kwargs):
        super(DllrfTable, self).__init__(*args, **kwargs)
        # self.info("start building a DllrfTable")
        self._outer_layout = QGridLayout(self)
        self._scroll = QScrollArea(self)
        self._scroll.setWidgetResizable(True)
        self._widget = QWidget()
        self._widget.setGeometry(QRect(0, 0, 384, 284))
        self._inner_layout = QGridLayout(self._widget)
        self._table = QGridLayout()
        self._table.setSpacing(0)
        self.setContentsMargins(0, 0, 0, 0)
        self._inner_layout.addLayout(self._table, 0, 0, 1, 1)
        self._scroll.setWidget(self._widget)
        self._outer_layout.addWidget(self._scroll, 0, 0, 1, 1)
        self._up_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum,
                                      QSizePolicy.Expanding)
        self._down_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum,
                                        QSizePolicy.Expanding)
        self._left_spacer = QSpacerItem(40, 20, QSizePolicy.Minimum,
                                        QSizePolicy.Minimum)
        self._right_spacer = QSpacerItem(40, 20, QSizePolicy.Minimum,
                                         QSizePolicy.Minimum)
        # columns 0 and 6 only has the spacer in row 1
        self._table.addItem(self._left_spacer, 1, 0, 1, 1)
        self._table.addItem(self._right_spacer, 1, 6, 1, 1)
        # upper spacer uses cell r=0, c=1: in the header row above the row label
        self._table.addItem(self._up_spacer, 0, 1, 1, 1)
        # down spacer temporary located in r=1, c=1.
        # Will be moved down when model is set
        self._table.addItem(self._down_spacer, 1, 1, 1, 1)
        # column 1 will have the row label that starts in 1
        # columns 2 to 5 have the headers for the coordinates
        for _j, _coordinate in enumerate(["I", "Q", "Amp", "Phase"], 2):
            _label = QLabel(self._widget)
            _label.setText(_coordinate)
            _label.setAlignment(QtCore_Qt.AlignCenter)
            self._table.addWidget(_label, 1, _j)
        #     self.info(f"label {_coordinate} at ({_i}, {_j})")
        # self.info(f"row {_i}: header done")
        TaurusBaseComponent.FORMAT = "{:.2f}"

    def setModel(self, model):
        """
        Alert: This isn't a normal taurus widget about how the model comes!

        The model expected is a dictionary where the keys are the rows and the
        values are dictionaries themselves with the 4 keys (I,Q,Amp,Phase) that
        are the columns of the table.
        In the leaf of this nested dict one can find the attribute name for
        each of the cells.

        That is to say model[row][column] = attribute_name_in_cell
        :param model: dict
        :return:
        """
        # self.info(f"setModel() with {len(model)} elements")
        if not isinstance(model, dict):
            raise TypeError("model expected is a dictionary with "
                            "the rows as keys")
        if self._model is not None:
            # self.info("setModel(): it is required to remove old widgets")
            # for each row, starting at 1, iterate for each column, starting
            # at 1, to remove all the widgets (columns of the label and 4
            # values) that exist from previous model.
            for _i in range(2, self._table.rowCount()):  # last row has a spacer
                for _j in range(1,6):
                    _widget = self._table.itemAtPosition(_i, _j)
                    self._table.removeWidget(_widget)
            # self.info("setModel(): table clean")
        # take out the down spacer to be placed once all the rows are placed
        self._table.removeItem(self._down_spacer)
        self._model = None
        _size_policy = QSizePolicy
        for _i, _row in enumerate(model.keys(), 2):
            # start in row 2 because 1 is the header
            _label = QLabel()
            _label.setText(_row)
            self._table.addWidget(_label, _i, 1)  # column 1 for the row name
            # self.info(f"{_i}th row {_row} label in place")
            for _j, _column in enumerate(model[_row], 2):
                # column with values starts at 2
                _cell_model = f"{model[_row][_column]}#rvalue.magnitude"
                # self.info(f"\trow[{_i}]: {_row:30}"
                #           f"\tcolumn[{_j}]: {_column:30}"
                #           f"\tcell: {_cell_model}")
                _widget = TaurusLabel(self._widget)
                _widget.setModel(_cell_model)
                self._table.addWidget(_widget, _i, _j)
                # self.info(f"\t{_j}th column: {_column} at ({_i}, {_j})")
        self._table.addItem(self._down_spacer, _i+1, 1)
        self._model = model

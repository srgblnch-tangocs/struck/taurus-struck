#!/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2021 CELLS / ALBA Synchrotron, Cerdanyola del Valles, Spain
#
# ##### END GPL LICENSE BLOCK #####


from functools import partial
from platform import python_version
from taurus.external.qt import Qt
from taurus import Device

"""
Used to do monkey patching of the About for the TaurusGui
"""

ABOUT_TEXT = 'About ...'  # Hackish: hardcoded
device_version_attributes = ['Version_CSV', 'Version_CodeGenerator',
                             'Version_DeviceServer']


def monkey_patch_about_action(taurus_gui_obj,
                              csv_version, codegen_version, device_name):
    help_menu = taurus_gui_obj.helpMenu  # assume about is in help
    position, _ = remove_about_action(help_menu, ABOUT_TEXT)
    add_newer_about_action(position, help_menu, taurus_gui_obj,
                           csv_version, codegen_version, device_name)


def remove_about_action(menu, title):
    for i, action in enumerate(menu.actions()):
        if action.text() == title:
            menu.removeAction(action)
            return i, action


def add_newer_about_action(position, menu, taurusmainwindow_obj,
                           csv_version, codegen_version, device_name):
    device = Device(device_name)
    attribute_values = {attribute.name: attribute.value
                        for attribute in
                        device.read_attributes(device_version_attributes)}
    self_showHelpAbout = partial(showHelpAbout, taurusmainwindow_obj,
                                 csv_version, codegen_version,
                                 device_name, attribute_values)
    first_action = menu.actions()[position]
    about_action = Qt.QAction("About ...", menu)
    about_action.triggered.connect(self_showHelpAbout)
    menu.insertAction(first_action, about_action)


def showHelpAbout(self, csv_version, codegen_version, device_name, device_info):
    device_version = device_info['Version_DeviceServer']
    device_codegen_version = device_info['Version_CodeGenerator']
    device_csv_version = device_info['Version_CSV']
    appname = str(Qt.qApp.applicationName())
    appversion = str(Qt.qApp.applicationVersion())
    from taurus.core import release
    abouttext = f"{appname}: {appversion}\n" \
                f"code generator: {codegen_version}\n" \
                f"csv source: {csv_version}\n\n" \
                f"device name: {device_name}\n" \
                f"device version: {device_version}\n" \
                f"device code generator: {device_codegen_version}\n" \
                f"device csv source: {device_csv_version}\n\n" \
                f"Using:\n" \
                f"{release.name} {release.version}\n" \
                f"python {python_version()}"
    Qt.QMessageBox.about(self, 'About', abouttext)

#!/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2021 CELLS / ALBA Synchrotron, Cerdanyola del Valles, Spain
#
# ##### END GPL LICENSE BLOCK #####

import click
from .constants import version
from os import path
from pkg_resources import EntryPoint
from taurus.cli import common, taurus_cmd
from taurus.qt.qtgui.taurusgui.taurusgui import gui_cmd
from taurus import Release


@click.group()
@common.log_level
@common.poll_period
@common.serial_mode
@common.default_formatter
@click.option(
    "--rconsole",
    "rconsole_port",
    type=click.INT,
    metavar="PORT",
    default=None,
    help="Enable remote debugging with rfoo on the given PORT",
)
@click.version_option(version=f"{version} with Taurus {Release.version}")
def taurus_cmd_part(log_level, polling_period, serialization_mode,
                    default_formatter, rconsole_port):
    taurus_cmd(log_level, polling_period, serialization_mode,
               default_formatter, rconsole_port)


# @click.command('gui')
# @click.option(
#     "--safe-mode",
#     "safe_mode",
#     is_flag=True,
#     default=False,
#     help=(
#         "launch in safe mode (it prevents potentially problematic "
#         + "configs from being loaded)"
#     ),
# )
# @click.option(
#     "--ini",
#     type=click.Path(exists=True),
#     metavar="INIFILE",
#     help=(
#         "settings file (.ini) to be loaded (defaults to "
#         + "<user_config_dir>/<appname>.ini)"
#     ),
#     default=None,
# )
# def taurus_gui_cmd_part(confname, safe_mode, ini):
#     gui_cmd(confname, safe_mode, ini)


def main():
    ep = EntryPoint('gui', 'taurus.qt.qtgui.taurusgui.taurusgui',
                    attrs=('gui_cmd',))
    taurus_cmd.add_command(ep.load())
    confname = f"{path.dirname(__file__)}/struck.py"
    taurus_cmd_part()
    # taurus_gui_cmd_part(confname)


if __name__ == '__main__':
    main()

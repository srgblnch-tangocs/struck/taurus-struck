# !/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2021 CELLS / ALBA Synchrotron, Cerdanyola del Valles, Spain
#
# ##### END GPL LICENSE BLOCK #####


from functools import partial
from operator import is_not
import re
from table import DllrfTable
from taurus import Device
from taurus.external.qt.QtCore import Qt as QtCore_Qt
from taurus.external.qt.QtWidgets import (QGridLayout, QSplitter)
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.panel import TaurusForm


COORDINATES_LIST = ['I', 'Q', 'Amp', 'Phase']


class DllrfPanel(TaurusWidget):
    _layout = None
    _splitter = None
    _table = None
    _form = None

    def __init__(self, withButtons=False, *args, **kwargs):
        # TaurusForm.__init__(self, *args, **kwargs)
        super(DllrfPanel, self).__init__(*args, **kwargs)
        self._layout = QGridLayout(self)
        self._layout.setObjectName("main_layout")
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._splitter = QSplitter(self)
        self._splitter.setOrientation(QtCore_Qt.Vertical)
        self._table = DllrfTable(self._splitter)
        self._form = TaurusForm(self._splitter)
        self.setWithButtons(withButtons)
        self._layout.addWidget(self._splitter, 0, 0, 1, 1)

    def setWithButtons(self, true_false):
        if true_false:
            self.warning("DllrfPanel is always without panel buttons")
        self._form.setWithButtons(False)

    def setModel(self, model):
        """
        From the given list of models, separate them in two groups. One for
        the quartets of models that corresponds to the four elements of the
        cartesian and polar coordinates of a component (aka I, Q, Amp, Phase).

        The second group are all the other attributes as well as the
        attributes that seems to be part of a quartet but it is found
        incomplete.

        Both groups are alphabetically sorted. The ones that represents
        coordinates are sorted by the root that doesn't include the prefix.
        :param model: list of strings
        :return: None
        """
        _coordinate_attributes = {}
        _other_attributes = []
        for _element in model:
            _device_name, _attribute_name = self._split_model(_element)
            _type, _pattern = self._get_coordinate_group(_attribute_name)
            if _pattern is not None:
                _pattern = _pattern.lower()
                if _pattern not in _coordinate_attributes:
                    _coordinate_attributes[_pattern] = {
                        'I': None, 'Q': None, 'Amp': None, 'Phase': None}
                _coordinate_attributes[_pattern][_type] = \
                    f"{_device_name}{_attribute_name}"
            else:
                _other_attributes.append(
                    f"{_device_name}{_attribute_name}")
        if len(_coordinate_attributes) > 0:
            _incomplete = self._check_incomplete_coordinate_attributes(
                _coordinate_attributes, _device_name)
            for _group in _incomplete:
                # This line does so much, so a descriptive comment is mandatory:
                # - remove the group from the dictionary using the pop,
                # - the returning object is an subdictionary with the 4 keys so,
                #   take the values of this dictionary as a list to then,
                # - filter to remove the None elements to finally have the list
                #   of attributes in the structure that have to be moved to the
                #   other_attributes
                _other_attributes += list(filter(
                    partial(is_not, None),
                    list(_coordinate_attributes.pop(_group).values())))
        _table_rows = list(_coordinate_attributes.keys())
        _table_rows.sort()
        _other_attributes.sort()
        # self.info(f"split complete with {len(_table_rows)} to be in the "
        #           f"table and {len(_other_attributes)} to be in a form")
        _merged_models = []
        for _group in _table_rows:
            _merged_models += list(_coordinate_attributes[_group].values())
        if len(_merged_models) == 0:
            self._table.hide()
        else:
            self._table.setModel(_coordinate_attributes)
        if len(_other_attributes) == 0:
            self._form.hide()
        else:
            self._form.setModel(_other_attributes)
        _merged_models += _other_attributes
        # TaurusForm.setModel(self, model)
        super(DllrfPanel, self).setModel(_merged_models)

    @staticmethod
    def _split_model(model):
        match = re.match("(.*/.*/.*/)(.*)", model)
        if match is None:
            # FIXME: this regexp doesn't interpret fqdn
            return None, model
        return list(match.groups())

    def _get_coordinate_group(self, name):
        for _type in COORDINATES_LIST:
            it_is, pattern = self._is_a_coordinate_attribute(name, _type)
            if it_is:
                return _type, pattern
        return None, None

    @staticmethod
    def _is_a_coordinate_attribute(name, _type):
        is_a_coordinate = re.match(f"{_type}(.*)", name)
        if is_a_coordinate is None:
            return False, ""
        return True, is_a_coordinate.groups()[0]

    def _check_incomplete_coordinate_attributes(self, attributes_dict,
                                                device_name):
        incomplete = []
        groups = list(attributes_dict.keys())
        groups.sort()
        # self.info(f"looking for incomplete groups in {groups}")
        for group in groups:
            elements = list(attributes_dict[group].values())
            if None in list(elements):
                # self.info(f"found incomplete '{group}': "
                #           f"{attributes_dict[group]}")
                if not self._try_autocomplete(device_name, group,
                                              attributes_dict):
                    # self.info(f"\tcouldn't complete")
                    incomplete.append(group)
                # else:
                #     self.info(f"\tit has been autocompleted: "
                #               f"{attributes_dict[group]}")
            # else:
            #     self.info(f"'{group}' is complete")
        return incomplete

    def _try_autocomplete(self, device_name, group, attributes_dict):
        try:
            _device = Device(device_name[:-1])
            attr_list = [attr.lower() for attr in
                         _device.get_attribute_list()]
            for coordinate in COORDINATES_LIST:
                if attributes_dict[group][coordinate] is None:
                    attribute_candidate = f"{coordinate}{group}".lower()
                    if attribute_candidate in attr_list:
                        # self.info(f"\t\t'{attribute_candidate}' found")
                        attributes_dict[group][coordinate] = \
                            f"{device_name}{attribute_candidate}"
                    # else:
                    #     self.info(f"\t\t'{attribute_candidate}' NOT found")
                # else:
                #     self.info(f"\t\t'{coordinate}{group}' already there "
                #               f"{attributes_dict[group][coordinate]}")
            return None not in list(attributes_dict[group].values())
        except AttributeError as exception:
            self.error(f"found an error trying to autocomplete {group}: "
                       f"{exception}")
            return False


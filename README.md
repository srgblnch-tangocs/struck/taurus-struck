# taurus-struck

![5 - Production](https://img.shields.io/badge/Development_Status-5_--_production-green.svg)

Licence: GPLv3+

release 1.2.1

**Table of contents**

[[_TOC_]]

---

## ToDo list

- [x] Panels like in the ctrflyrtech
  - [x] Start with the two types: diagnostics/settings
  - [x] Further than types, use grouping
    - [x] attributes can be in more than one group
  - [x] Start with TaurusForm panels
    - [x] Specialize the panels with grid views to provide more compact
      vision to the use
  - [ ] When the device supports state/status-like attributes (with a string 
    list with the possible values) use that to show combobox as write widget
- [ ] Commands panel (Start/Stop device commands) or perhaps in the 
  quickAccessToolBar
- [x] Trend panel
- [x] JorgBar with Alba's logo together with the taurus logo
  - [ ] device state led, perhaps in the JorgBar or in the quickAccessToolBar
- [x] Menu about: add versions of the submodules and device info
- [ ] Instead of hardcode the device, list attributes and search for devices
  and show a combo to change between them (perhaps in the quickAccessToolBar)
  - [ ] combo with a "key:value" to mask the device name and show user label
- [x] attribute precision to 2 decimals 

## prepare

### git submodules

After clone the repo, it is necessary to get the submodules:

```bash
git submodule init
git submodule update
```

There are two submodules in this project. One provides de 
[registers](https://gitlab.com/srgblnch-tangocs/struck/registers) in a 
```csv``` description and the other is the 
[codegenerator](https://gitlab.com/srgblnch-tangocs/struck/codegenerator),  
the tool to convert this description to code that is then used within the 
device server as well as it is used by the graphical interface.

### conda environment

Then it is also necessary to set up a conda environment.

```bash
conda env create -f environment.yml
```

In development environment use the ```environment-dev.yml``` file

Having two different environments, one for the development and another for the 
production, we can provide a cleaner way to work.

If any package in the environment has to be changed via conda, then the yaml 
files needs to be updated:

```bash
conda env export | grep -v "^prefix: " > environment.yml
```

To build the environments from scratch one can call:

```bash
conda create -n taurus-struck python=3.10 taurus=5.0 taurus_pyqtgraph=0.5.9 cpptango=9.3.4 pytango=9.3.3 -c conda-forge
conda create -n taurus-struck-dev python=3.10 taurus=5.0 taurus_pyqtgraph=0.5.9 cpptango=9.3.4 pytango=9.3.3 ipython=7.29.0 -c conda-forge
```

### code generation

The repo already includes the sources of a generated device. So this step is 
only necessary in case of modifications in the csv file.

(**FIXME**): this is not working with python 3.9, use python2.7 until fix
```bash
python ctrfdll/codegenerator/codegenerator.py --type gui --device bessy/rf/struck-01 ctrfdll/registers/struck.csv ctrfdll/struck.py
```

## Installation

It is suggested to use the development environment to generate the code, 
to then create a wheel python package that is then installed in the production
environment.

```bash
conda env create -f environment-dev.yml
conda activate taurus-struck-dev
python setup.py bdist_wheel
conda deactivate
conda env create -f environment.yml
conda activate taurus-struck
pip install dist/taurus_struck-1.0.X-py3-none-any.whl
```

In case of a development version, it is suggested to use the dev environment 
to install the wheels file.

```bash
conda env create -f environment-dev.yml
conda activate taurus-struck-dev
python setup.py bdist_wheel
pip install dist/taurus_struck-1.0.XaY-py3-none-any.whl
```

## Usage

There is a script in ```launcher/ctrfdll``` that prepares to use the correct 
environment and launches the taurus gui generated. There is another launcher 
```launcher/ctrfdll-dev``` that uses the development environment just to 
check the gui when one is in an alpha release.

This launcher could be placed in the `PATH`. As an example in 
`$HOME/anaconda/bin`
